package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPageHomework extends LoginPage {
    private WebDriver driver;
    WebDriverWait wait;
    private Actions clickYourRegistrationButton;


    public LoginPageHomework(WebDriver driver) {
        super(driver);
    }

    public static void login(String firstname, String lastname, String email, String username, String password, String confirmpassword) {
    }


    public void openLoginPageHomework(String hostname) {
        
        System.out.println("Open the next url:" + hostname + "/stubs/auth.html");
        driver.get(hostname + "/stubs/auth.html");
        driver.findElement(By.id("register-submit"));
        clickYourRegistrationButton.click();
    }


}